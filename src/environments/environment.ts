// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
   firebaseConfig : {
    apiKey: "AIzaSyAAjHO6Jzo8_oQKHwN2hBinmp45bnI86a0",
    authDomain: "ang-homework2.firebaseapp.com",
    databaseURL: "https://ang-homework2.firebaseio.com",
    projectId: "ang-homework2",
    storageBucket: "ang-homework2.appspot.com",
    messagingSenderId: "934015151499",
    appId: "1:934015151499:web:31612878bef220cd1dd38e",
    measurementId: "G-7XHGWDTDBE"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
