import { Router } from '@angular/router';
import { AngularFireAuth } from '@angular/fire/auth';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
 import { User } from './interfaces/user';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: Observable<User| null>
 
  constructor(public afAuth:AngularFireAuth,private router:Router) { 
   this.user = this.afAuth.authState;
}

getUser(){
  return this.user 
}

signup(email:string, password:string){
     this.afAuth
     .auth.createUserWithEmailAndPassword(email,password)
     .then
     (res => { console.log('Succesful Signup', res);
     this.router.navigate(['/posts']);
    }
     )
  }
    signin(email:string, password:string){
      this.afAuth
      .auth.signInWithEmailAndPassword(email,password)
      .then(
        res => { console.log('Succesful login', res);
      this.router.navigate(['/posts']);
     }
      )
    }
    logout(){
      this.afAuth.auth.signOut();
     }
}