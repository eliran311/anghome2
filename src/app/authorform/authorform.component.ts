import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-authorform',
  templateUrl: './authorform.component.html',
  styleUrls: ['./authorform.component.css']
})
export class AuthorformComponent implements OnInit {

  constructor(private router: Router,private route: ActivatedRoute) { }
author;
id;
onSubmit(){
this.router.navigate(['/authors',this.id,this.author]);
}
ngOnInit() {
this.id=this.route.snapshot.params.id;
this.author=this.route.snapshot.params.author;
}

}

