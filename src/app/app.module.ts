import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule, ReactiveFormsModule }   from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavComponent } from './nav/nav.component';
import { LayoutModule } from '@angular/cdk/layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatListModule } from '@angular/material/list';
import { BooksComponent } from './books/books.component';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatCardModule} from '@angular/material/card';
import { AuthorsComponent } from './authors/authors.component';
import { RouterModule, Routes } from '@angular/router';
import { AuthorformComponent } from './authorform/authorform.component';
import { PostsComponent } from './posts/posts.component';
import { HttpClientModule } from '@angular/common/http';
import { AngularFireModule } from '@angular/fire';
import { environment } from '../environments/environment';
import {MatFormFieldModule} from '@angular/material/form-field';

import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PostformComponent } from './postform/postform.component';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatRadioModule } from '@angular/material/radio';
import { SignupComponent } from './signup/signup.component';
import { LoginComponent } from './login/login.component';
import { DocformComponent } from './docform/docform.component';
import { ClassifiedComponent } from './classified/classified.component';
import { ClassifiedpackComponent } from './classifiedpack/classifiedpack.component';
import { MatTableModule } from '@angular/material';

const appRoutes: Routes = [
  { path: 'books', component: BooksComponent },
  { path: 'authors/:id/:author',component: AuthorsComponent}, 
  // { path: 'editauthors/:id/:author', component: AuthorformComponent },
  { path: 'posts', component: PostsComponent },
  { path: 'postform', component: PostformComponent },
  { path: 'postform/:id', component: PostformComponent },
  { path: 'signup', component: SignupComponent },
  { path: 'login', component: LoginComponent },
  { path: 'classify', component: DocformComponent},
  { path: 'classified', component: ClassifiedComponent},
  { path: 'classifiedsave', component: ClassifiedpackComponent},
  { path: '',
    redirectTo: '/books',
    pathMatch: 'full'
  },
];

@NgModule({
  declarations: [
    AppComponent,
    NavComponent,
    BooksComponent,
    AuthorsComponent,
    AuthorformComponent,
    PostsComponent,
    PostformComponent,
    SignupComponent,
    LoginComponent,
    DocformComponent,
    ClassifiedComponent,
    ClassifiedpackComponent,
   
  ],
  
  imports: [
    AngularFireModule.initializeApp(environment.firebaseConfig),
    RouterModule.forRoot(appRoutes,{ enableTracing: true }), // <-- debugging purposes only
    BrowserModule,
    FormsModule,
    BrowserAnimationsModule, 
    LayoutModule, 
    MatToolbarModule, 
    MatButtonModule, 
    MatTableModule, 
    MatSidenavModule, 
    MatFormFieldModule,
    MatIconModule,
    MatCardModule,
    HttpClientModule,
    MatExpansionModule, 
    MatListModule,
    AngularFirestoreModule,
    AngularFireStorageModule,
    AngularFireAuthModule,
    RouterModule,
    RouterModule.forRoot(
      appRoutes,
      { enableTracing: true } // <-- debugging purposes only
    ),
    MatInputModule,
    MatSelectModule,
    MatRadioModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
