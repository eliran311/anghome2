import { AuthorsService } from './../authors.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  // author;
  // id;
  authors$:Observable<any>;
  author:string; 
  constructor(private route: ActivatedRoute, private Authorsservice:AuthorsService) { }
onSubmit(){
  this.Authorsservice.addAuthors(this.author);
}
  ngOnInit() {
    this.authors$ = this.Authorsservice.getAuthors();
    // this.author = this.route.snapshot.params.author;
    // this.id = this.route.snapshot.params.id;
  }

}
