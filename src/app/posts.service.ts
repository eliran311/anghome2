import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule  } from '@angular/common/http';
import { Observable} from 'rxjs';
import { Posts } from './interfaces/posts';
import { map } from 'rxjs/internal/operators/map';
import { Users } from './interfaces/users';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { AuthService } from './auth.service';
@Injectable({
  providedIn: 'root'
})
export class PostsService {
  
userCollection:AngularFirestoreCollection = this.db.collection('users');
postCollection:AngularFirestoreCollection
private URL = "https://jsonplaceholder.typicode.com/posts/";
private URL1 = "https://jsonplaceholder.typicode.com/users/";

getPosts(userId): Observable<any[]>{
  this.postCollection = this.db.collection(`users/${userId}/posts`);
  console.log('Posts collection created');
  // return this.db.collection('posts').valueChanges(({idField:'id'}));
  return this.postCollection.snapshotChanges().pipe(
    map(collection => collection.map(document => {
      const data = document.payload.doc.data();
      data.id = document.payload.doc.id;
      return data;
    }))
  );
   }
   getPost(userId,id:string):Observable<any>{
    return this.db.doc(`users/${userId}/posts/${id}`).get()
  }
  deletePost(userId,id:string){
    this.db.doc(`users/${userId}/posts/${id}`).delete();
  }
  
  updatePost(userId,id:string, title:string, body:string, author:string){
    this.db.doc(`users/${userId}/posts/${id}`).update({
      title:title,
      body:body,
      author:author
    })
  }
/*getPosts(){
  return this.http.get<Posts[]>(this.URL);
   }
               
getUsers(){
  return this.http.get<Users[]>(this.URL1);
    }*/
   /*getposts():Observable<Posts[]> {
     return this.http.get(this.URL).pipe(map((response:Posts[]) => response));
         
   };*/
   /*getusers():Observable<Users[]> {
    return this.http.get(this.URL1).pipe(map((response:Users[]) => response));
         
  };*/
  addPosts(userId,title:string, body:string, author:string){
         const post = {title:title, body:body, author:author}
        //  this.db.collection('posts').add(post);
         this.userCollection.doc(userId).collection('posts').add(post);
      }
  constructor(private http: HttpClient,  private db:AngularFirestore,private authService:AuthService) { }
}
