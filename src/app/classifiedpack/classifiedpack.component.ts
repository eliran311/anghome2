import { ClassifyService } from './../classify.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthService } from '../auth.service';



@Component({
  selector: 'app-classifiedpack',
  templateUrl: './classifiedpack.component.html',
  styleUrls: ['./classifiedpack.component.css']
})
export class ClassifiedpackComponent implements OnInit {
  classify$:Observable<any>;
  userId;
 
  
  constructor(public classifyService:ClassifyService,public authService:AuthService) { }

  ngOnInit() {
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
    this.classify$ = this.classifyService.getClassifys(this.userId);
    
  })

}
}
