import { Component, OnInit } from '@angular/core';
import { PostsService } from '../posts.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-postform',
  templateUrl: './postform.component.html',
  styleUrls: ['./postform.component.css']
})
export class PostformComponent implements OnInit {
  id:string;
  title:string;
  body:string;
  author:string;
  userId:string;
  isEdit:boolean = false;
  buttonText:string = "Add Post";

  constructor(private postsservice:PostsService,
    private authService:AuthService, 
    private router:Router,
    private route:ActivatedRoute) { }
       

  ngOnInit() {
    this.id = this.route.snapshot.params.id; 
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;    
      console.log(this.id);
  if (this.id){
    this.isEdit = true;
    this.buttonText = "Update Post";
    console.log(this.buttonText);
    this.postsservice.getPost(this.userId,this.id).subscribe(
      post => {
        this.title = post.data().title;
        this.body = post.data().body;
        this.author = post.data().author;
      })
    }
    })
}

  onSubmit(){
    if(this.isEdit){
      this.postsservice.updatePost(this.userId,this.id,this.title,this.body, this.author)
    }
    else{
    this.postsservice.addPosts(this.userId,this.title,this.body, this.author)
    }
    this.router.navigate(['/posts']);
  
}
}