import { PostsService } from './../posts.service';
import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Posts } from '../interfaces/posts';
import { Users } from '../interfaces/users';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
// posts$:Posts;
panelOpenState = false;
posts$: Observable<any[]>;
userId;
// posts$:Observable<Posts[]>  
// users$:Observable<Users[]>
/*
posts$: Posts[]=[];
users$: Users[]=[];
body:string;
title:string;
author:string;
pushAlert:string;
*/
constructor(private Postsservice:PostsService,public authService:AuthService) { }
/*
pushPosts(){
  for(let i=0;i<this.posts$.length;i++){
    for(let j=0;j<this.users$.length;j++){
      if(this.posts$[i].userId==this.users$[j].id){
        this.body = this.posts$[i].body;
        this.title = this.posts$[i].title;
        this.author = this.users$[j].name;
        this.Postsservice.addPosts(this.body,this.title,this.author);
      }
    }
  }
  this.pushAlert = "Posts have been saved to FireStore"
}*/
  ngOnInit() {
    console.log("NgOnInit started") ;
    this.authService.getUser().subscribe(
      user => {
        this.userId = user.uid;
        this.posts$ = this.Postsservice.getPosts(this.userId); 
      }
    )
    
    /* this.Postsservice.getPosts().subscribe(data => this.posts$ = data);;
     this.Postsservice.getUsers().subscribe(data => this.users$ = data);;
  */
    }
    deletePost(id:string){
      this.Postsservice.deletePost(this.userId,id);
    }
}
